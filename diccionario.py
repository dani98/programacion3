if __name__ == "__main__":
    print("Mi diccionario")
    diccionario = {
        "raton" : ["mouse","Mamífero roedor de pequeño tamaño, pelo fino, cola larga, patas cortas, cabeza pequeña y orejas tiesas .\n","Small rodent mammal, thin hair, long tail, short legs, small head and stiff ears\n"],
        "pantalla" : ["screen","Superficie blanca, plana y lisa, de materia textil o plástico, sobre la que se proyectan imágenes cinematográficas o fotográficas.\n","White, flat and smooth surface, of textile or plastic, on which cinematographic or photographic images are projected"],
        "pantalon" : ["pants","Prenda de vestir que se ajusta a la cintura y llega a una altura variable de la pierna o hasta los tobillos, cubriendo cada pierna por separado.\n","Garment that conforms to the waist and reaches a variable height of the leg or to the ankles, covering each leg separately."],
    }

    print(diccionario)
    print("Desea ver el segundo diccionario?(s/n)\n")
    res = input(' ')
    if res.lower() == 's' or res == 'S':
        diccionario.clear()
        diccionario2 = {
            "Saludar": ["Greet",
                        "Es un acto comunicacional en que una persona hace notar a otra su presencia generalmente a través del habla o de algún gesto.",
                        "It is a communicational act in which a person notices another person's presence usually through speech or some gesture."],
            "Computadora": ["Computer", "Es una máquina que está diseñada para facilitarnos la vida.",
                            "It is a machine that is designed to make our lives easier."]
        }
        print(diccionario2)
        print (diccionario2["Computadora"])
    else:
        print("Adios")
